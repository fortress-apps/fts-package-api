<?php

namespace Fortress\Api\Hydrator;

interface HydratorInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function hydrate(array $data);
}
