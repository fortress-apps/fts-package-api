<?php

namespace Fortress\Api\Request;

class PostRequest extends RequestAbstract implements RequestInterface
{
    /**
     * @var string
     */
    protected $method = 'POST';

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }
}
