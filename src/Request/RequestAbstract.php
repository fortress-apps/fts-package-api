<?php

namespace Fortress\Api\Request;

abstract class RequestAbstract
{
    protected string $uri;
    protected array $data = [];
    protected array $headers = [];

    /**
     * PostRequest constructor.
     *
     * @param string $uri
     * @param array  $data
     * @param array  $headers
     */
    public function __construct(string $uri, array $data = [], array $headers = [])
    {
        $this->uri = $uri;
        $this->data = $data;
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return array
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    abstract public function getMethod(): string;
}
