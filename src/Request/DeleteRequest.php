<?php

namespace Fortress\Api\Request;

class DeleteRequest extends RequestAbstract implements RequestInterface
{
    /**
     * @var string
     */
    protected $method = 'DELETE';

    /**
     * DeleteRequest constructor.
     *
     * @param string $uri
     * @param array  $headers
     */
    public function __construct(string $uri, array $headers = [])
    {
        parent::__construct($uri, [], $headers);
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }
}
