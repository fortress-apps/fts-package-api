<?php

namespace Fortress\Api\Request\Query;

class QueryBuilderRequestQuery extends RequestQuery
{
    public function addIncludeCount($include): QueryBuilderRequestQuery
    {
        if (is_array($include)) {
            $include = implode(',', $include);
        }

        $includeCount = [
            'includeCount' => $include
        ];

        $this->queryParams = array_merge($this->queryParams, $includeCount);

        return $this;
    }

    public function addInclude($include): QueryBuilderRequestQuery
    {
        if (is_array($include)) {
            $include = implode(',', $include);
        }

        $includeRelationships = [
            'include' => $include
        ];

        $this->queryParams = array_merge($this->queryParams, $includeRelationships);

        return $this;
    }

    public function addFilter(array $filters): QueryBuilderRequestQuery
    {
        $includeFilters = [
            'filter' => $filters
        ];

        $this->queryParams = array_merge($this->queryParams, $includeFilters);

        return $this;
    }

    public function addSort(array $sort): QueryBuilderRequestQuery
    {
        $includeSort = [
            'sort' => $sort
        ];

        $this->queryParams = array_merge($this->queryParams, $includeSort);

        return $this;
    }

    public function addLimit(int $limit): QueryBuilderRequestQuery
    {
        $this->queryParams = array_merge($this->queryParams, [
            'limit' => $limit,
        ]);

        return $this;
    }
}
