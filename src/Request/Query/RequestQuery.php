<?php

namespace Fortress\Api\Request\Query;

use Fortress\Api\Hydrator\HydratorInterface;

class RequestQuery
{
    protected string $path;
    protected ?array $queryParams;
    protected ?array $headers;
    protected ?HydratorInterface $hydrator;

    public function __construct(
        string $path,
        ?HydratorInterface $hydrator = null,
        ?array $queryParams = [],
        ?array $headers = []
    ) {
        $this->path = $path;
        $this->hydrator = $hydrator;
        $this->queryParams = $queryParams ?? [];
        $this->headers = $headers ?? [];
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getHydrator():? HydratorInterface
    {
        return $this->hydrator;
    }

    public function getUrl(): string
    {
        if (empty($this->getQueryParams())) {
            return $this->getPath();
        }

        return sprintf(
            '%s?%s',
            $this->getPath(),
            http_build_query($this->getQueryParams())
        );
    }

    public function setQueryParams(array $queryParams): RequestQuery
    {
        $this->queryParams = $queryParams;

        return $this;
    }

    public function setPage(int $page): RequestQuery
    {
        $this->queryParams = array_merge($this->queryParams, [
            'page' => $page
        ]);

        return $this;
    }

    public function setAuthHeader(string $token, string $type = 'Bearer'): RequestQuery
    {
        $this->headers['Authorization'] = sprintf('%s %s', $type, $token);

        return $this;
    }
}
