<?php

namespace Fortress\Api\Request\Query;

use Fortress\Api\Hydrator\HydratorInterface;

class StoreRequestQuery extends RequestQuery
{
    protected array $data;

    public function __construct(
        string $path,
        array $data,
        ?HydratorInterface $hydrator = null,
        ?array $queryParams = [],
        ?array $headers = []
    ) {
        parent::__construct($path, $hydrator, $queryParams, $headers);

        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
