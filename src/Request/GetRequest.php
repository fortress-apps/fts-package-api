<?php

namespace Fortress\Api\Request;

class GetRequest extends RequestAbstract implements RequestInterface
{
    /**
     * @var string
     */
    protected $method = 'GET';

    public function __construct(string $uri, array $headers = [])
    {
        parent::__construct($uri, [], $headers);
    }


    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }
}
