<?php

namespace Fortress\Api\Request;

class PutRequest extends RequestAbstract implements RequestInterface
{
    /**
     * @var string
     */
    protected $method = 'PUT';

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }
}
