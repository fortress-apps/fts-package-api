<?php

namespace Fortress\Api\Request;

class PatchRequest extends RequestAbstract implements RequestInterface
{
    /**
     * @var string
     */
    protected $method = 'PATCH';

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }
}
