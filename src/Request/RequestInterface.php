<?php

namespace Fortress\Api\Request;

interface RequestInterface
{
    const HEADER_AUTHORIZATION = 'Authorization';
    const HEADER_ACCEPT = 'Accept';
    const HEADER_CONTENT_TYPE = 'Content-Type';

    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @return string
     */
    public function getUri(): string;

    /**
     * @return array
     */
    public function getData(): ?array;

    /**
     * @return array
     */
    public function getHeaders(): array;
}
