<?php

namespace Fortress\Api\Mapper;

use Fortress\Api\Request\Query\RequestQuery;
use Fortress\Api\Request\Query\StoreRequestQuery;

interface RequestMapperInterface
{
    /**
     * @param RequestQuery $requestQuery
     *
     * @return object
     * @throws \Exception
     */
    public function get(RequestQuery $requestQuery);

    /**
     * @param StoreRequestQuery $requestQuery
     *
     * @return object
     */
    public function create(StoreRequestQuery $requestQuery);

    /**
     * @param StoreRequestQuery $requestQuery
     *
     * @return object
     */
    public function update(StoreRequestQuery $requestQuery);

    /**
     * @param StoreRequestQuery $requestQuery
     *
     * @return object
     */
    public function patch(StoreRequestQuery $requestQuery);

    /**
     * @param RequestQuery $requestQuery
     *
     * @return object
     * @throws \Exception
     */
    public function delete(RequestQuery $requestQuery);
}
