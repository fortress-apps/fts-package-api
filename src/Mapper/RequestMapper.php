<?php

namespace Fortress\Api\Mapper;

use Fortress\Api\Request\GetRequest;
use Fortress\Api\Request\PatchRequest;
use Fortress\Api\Request\PostRequest;
use Fortress\Api\Request\PutRequest;
use Fortress\Api\Request\DeleteRequest;
use Fortress\Api\Request\Query\RequestQuery;
use Fortress\Api\Request\Query\StoreRequestQuery;
use Fortress\Api\Request\RequestInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RequestMapper implements RequestMapperInterface
{
    private MapperInterface $mapper;

    /**
     * ApiClient constructor.
     *
     * @param MapperInterface $mapper
     */
    public function __construct(MapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function get(RequestQuery $requestQuery)
    {
        $request = new GetRequest(
            $requestQuery->getUrl(),
            $requestQuery->getHeaders()
        );

        return $this->map($request, $requestQuery);
    }

    /**
     * {@inheritdoc}
     */
    public function create(StoreRequestQuery $requestQuery)
    {
        $request = new PostRequest(
            $requestQuery->getUrl(),
            $requestQuery->getData(),
            $requestQuery->getHeaders()
        );

        return $this->map($request, $requestQuery);
    }

    /**
     * {@inheritdoc}
     */
    public function update(StoreRequestQuery $requestQuery)
    {
        $request = new PutRequest(
            $requestQuery->getUrl(),
            $requestQuery->getData(),
            $requestQuery->getHeaders()
        );

        return $this->map($request, $requestQuery);
    }

    /**
     * {@inheritdoc}
     */
    public function patch(StoreRequestQuery $requestQuery)
    {
        $request = new PatchRequest(
            $requestQuery->getUrl(),
            $requestQuery->getData(),
            $requestQuery->getHeaders()
        );

        return $this->map($request, $requestQuery);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(RequestQuery $requestQuery)
    {
        $request = new DeleteRequest(
            $requestQuery->getUrl(),
            $requestQuery->getHeaders()
        );

        return $this->map($request, $requestQuery);
    }

    /**
     * @param RequestInterface $request
     * @param RequestQuery     $requestQuery
     *
     * @return object
     */
    protected function map(RequestInterface $request, RequestQuery $requestQuery)
    {
        try {
            return $this->mapper->map($request, $requestQuery->getHydrator());
        } catch (ClientException $clientException) {
            if ($clientException->getCode() == Response::HTTP_FORBIDDEN) {
                $this->throwNotFound($requestQuery->getUrl());
            }

            throw $clientException;
        }
    }

    /**
     * @param string $requestUrl
     */
    protected function throwNotFound(string $requestUrl)
    {
        throw new NotFoundHttpException(sprintf(
            "Resource not found not found at %s",
            $requestUrl
        ));
    }
}
