<?php

namespace Fortress\Api\Mapper;

use Fortress\Api\Hydrator\HydratorInterface;
use Fortress\Api\Request\RequestInterface;
use GuzzleHttp\Exception\ClientException;

interface MapperInterface
{
    /**
     * @param RequestInterface $request
     * @param HydratorInterface $hydrator
     *
     * @return object
     * @throws ClientException
     */
    public function map(RequestInterface $request, HydratorInterface $hydrator = null);
}
