<?php

namespace Fortress\Api\Mapper;

use Fortress\Api\Http\HttpClientInterface;
use Fortress\Api\Hydrator\HydratorInterface;
use Symfony\Component\HttpFoundation\Response;
use Fortress\Api\Request\RequestInterface;

class EntityMapper implements MapperInterface
{
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function map(RequestInterface $request, HydratorInterface $hydrator = null)
    {
        $response = $this->client->makeRequest($request);

        if (!$hydrator instanceof HydratorInterface || $response->getStatusCode() == Response::HTTP_NO_CONTENT) {
            return $response;
        }

        return $hydrator->hydrate(json_decode($response->getBody(), true));
    }
}
