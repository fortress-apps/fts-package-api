<?php

namespace Fortress\Api;

use Fortress\Api\Http\HttpJsonClient;
use Fortress\Api\Mapper\EntityMapper;
use Fortress\Api\Mapper\RequestMapper;
use Fortress\Api\Mapper\RequestMapperInterface;
use GuzzleHttp\Client;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function boot(): void
    {
        $this->publishes([
            realpath(__DIR__.'/config/api.php') => config_path('api.php'),
        ]);
    }

    public function register(): void
    {
        $this->app->singleton('api.http_client.guzzle', function () {
            return new Client([
                'base_uri' => config('api.api_url'),
            ]);
        });

        $this->app->singleton('api.http_client.json', function (Application $application) {
            $client = $application->make('api.http_client.guzzle');

            return new HttpJsonClient($client);
        });

        $this->app->singleton('api.entity_mapper', function (Application $application) {
            return new EntityMapper($application->make('api.http_client.json'));
        });

        $this->app->singleton('api.request_mapper', function (Application $app) {
            return new RequestMapper($app->make('api.entity_mapper'));
        });
        $this->app->bind(RequestMapperInterface::class, 'api.request_mapper');
    }
}
