<?php

namespace Fortress\Api\Http;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Fortress\Api\Request\RequestInterface;

class HttpJsonClient extends HttpClientAbstract implements HttpClientInterface
{
    private array $defaultHeaders = [
        RequestInterface::HEADER_CONTENT_TYPE => 'application/json',
        RequestInterface::HEADER_ACCEPT => 'application/json'
    ];

    /**
     * {@inheritDoc}
     */
    public function makeRequest(RequestInterface $request): ResponseInterface
    {
        $options = [
            RequestOptions::HEADERS => array_merge($this->defaultHeaders, $request->getHeaders())
        ];

        $data = $request->getData();

        if (!empty($data)) {
            $options[RequestOptions::JSON] = $request->getData();
        }

        return $this->client->request($request->getMethod(), $request->getUri(), $options);
    }
}
