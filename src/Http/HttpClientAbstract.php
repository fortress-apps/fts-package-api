<?php

namespace Fortress\Api\Http;

use GuzzleHttp\ClientInterface;

abstract class HttpClientAbstract
{
    protected ClientInterface $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }
}
