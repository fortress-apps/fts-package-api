<?php

namespace Fortress\Api\Http;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Fortress\Api\Request\RequestInterface;

interface HttpClientInterface
{
    /**
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function makeRequest(RequestInterface $request): ResponseInterface;
}
