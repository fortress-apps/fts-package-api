<?php

namespace Fortress\Api\Tests\Request;

use Fortress\Api\Request\RequestAbstract;
use PHPUnit\Framework\TestCase;

class RequestAbstractTest extends TestCase
{
    public function testAbstractMethods()
    {
        $sut = $this->getMockForAbstractClass(RequestAbstract::class, [
            '/test',
            ['data' => 'test'],
            ['headers' => 'test']
        ]);

        $this->assertEquals('/test', $sut->getUri());
        $this->assertEquals(['data' => 'test'], $sut->getData());
        $this->assertEquals(['headers' => 'test'], $sut->getHeaders());
    }
}
