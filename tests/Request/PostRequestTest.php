<?php

namespace Fortress\Api\Tests\Request;

use Fortress\Api\Request\PostRequest;
use PHPUnit\Framework\TestCase;

class PostRequestTest extends TestCase
{
    public function testMethod()
    {
        $sut = new PostRequest('/test');

        $this->assertEquals('POST', $sut->getMethod());
    }
}
