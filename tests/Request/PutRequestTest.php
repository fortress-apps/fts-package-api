<?php

namespace Fortress\Api\Tests\Request;

use Fortress\Api\Request\PutRequest;
use PHPUnit\Framework\TestCase;

class PutRequestTest extends TestCase
{
    public function testMethod()
    {
        $sut = new PutRequest('/test');

        $this->assertEquals('PUT', $sut->getMethod());
    }
}
