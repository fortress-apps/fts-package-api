<?php

namespace Fortress\Api\Tests\Request;

use Fortress\Api\Request\PatchRequest;
use PHPUnit\Framework\TestCase;

class PatchRequestTest extends TestCase
{
    public function testMethod()
    {
        $sut = new PatchRequest('/test');

        $this->assertEquals('PATCH', $sut->getMethod());
    }
}
