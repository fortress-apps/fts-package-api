<?php

namespace Fortress\Api\Tests\Request\Query;

use Fortress\Api\Hydrator\HydratorInterface;
use Fortress\Api\Request\Query\QueryBuilderRequestQuery;
use PHPUnit\Framework\TestCase;

class QueryBuilderRequestQueryTest extends TestCase
{
    public function testRequestQuery()
    {
        $path = '/test';
        $hydrator = $this->createMock(HydratorInterface::class);
        $queryParams = ['test' => 1,];
        $headers = ['X-TEST-HEADER' => 'TESTING'];
        $testToken = 'testtoken1234';
        $request = new QueryBuilderRequestQuery($path, $hydrator, $queryParams, $headers);
        $request->setAuthHeader($testToken);
        $request->addIncludeCount(['users', 'errors']);
        $request->addInclude(['comments','likes']);
        $request->addFilter(['approved' => 1]);
        $request->addSort(['created_at' => 'desc']);
        $request->addLimit(10);

        $this->assertEquals($path, $request->getPath());
        $this->assertEquals([
            'test' => 1,
            'includeCount' => 'users,errors',
            'include' => 'comments,likes',
            'filter' => ['approved' => 1],
            'sort' => ['created_at' => 'desc'],
            'limit' => 10
        ], $request->getQueryParams());
        $this->assertEquals(sprintf(
            '%s?%s',
            $path,
            http_build_query($request->getQueryParams())
        ), $request->getUrl());
        $this->assertEquals($hydrator, $request->getHydrator());
        $this->assertEquals([
            'X-TEST-HEADER' => 'TESTING',
            'Authorization' => 'Bearer testtoken1234'
        ], $request->getHeaders());
    }
}
