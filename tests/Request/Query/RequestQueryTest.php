<?php

namespace Fortress\Api\Tests\Request\Query;

use Fortress\Api\Hydrator\HydratorInterface;
use Fortress\Api\Request\Query\RequestQuery;
use PHPUnit\Framework\TestCase;

class RequestQueryTest extends TestCase
{
    public function testRequestQuery()
    {
        $path = '/test';
        $hydrator = $this->createMock(HydratorInterface::class);
        $queryParams = ['test' => 1,];
        $headers = ['X-TEST-HEADER' => 'TESTING'];
        $testToken = 'testtoken1234';
        $request = new RequestQuery($path, $hydrator, $queryParams, $headers);
        $request->setAuthHeader($testToken);

        $this->assertEquals($path, $request->getPath());
        $this->assertEquals(sprintf('%s?%s', $path, 'test=1'), $request->getUrl());
        $this->assertEquals($queryParams, $request->getQueryParams());
        $this->assertEquals($hydrator, $request->getHydrator());
        $this->assertEquals([
            'X-TEST-HEADER' => 'TESTING',
            'Authorization' => 'Bearer testtoken1234'
        ], $request->getHeaders());
    }
}
