<?php

namespace Fortress\Api\Tests\Request;

use Fortress\Api\Request\GetRequest;
use PHPUnit\Framework\TestCase;

class GetRequestTest extends TestCase
{
    public function testMethod()
    {
        $sut = new GetRequest('/test');

        $this->assertEquals('GET', $sut->getMethod());
    }
}
