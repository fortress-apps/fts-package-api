<?php

namespace Fortress\Api\Tests\Request;

use Fortress\Api\Request\DeleteRequest;
use PHPUnit\Framework\TestCase;

class DeleteRequestTest extends TestCase
{
    public function testMethod()
    {
        $sut = new DeleteRequest('/test');

        $this->assertEquals('DELETE', $sut->getMethod());
    }
}
