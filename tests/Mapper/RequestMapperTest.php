<?php

namespace Fortress\Api\Tests\Mapper;

use Fortress\Api\Mapper\MapperInterface;
use Fortress\Api\Mapper\RequestMapper;
use Fortress\Api\Request\DeleteRequest;
use Fortress\Api\Request\GetRequest;
use Fortress\Api\Request\PatchRequest;
use Fortress\Api\Request\PostRequest;
use Fortress\Api\Request\PutRequest;
use Fortress\Api\Request\Query\RequestQuery;
use Fortress\Api\Request\Query\StoreRequestQuery;
use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Exception\ClientException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RequestMapperTest extends TestCase
{
    protected RequestQuery $requestQuery;
    protected StoreRequestQuery $storeRequestQuery;
    protected MockObject $mapper;
    protected RequestMapper $sut;

    protected function setUp(): void
    {
        parent::setUp();

        $this->requestQuery = new RequestQuery('/test');
        $this->storeRequestQuery = new StoreRequestQuery('/test', ['enabled' => 1]);
        $this->mapper = $this->createMock(MapperInterface::class);
        $this->sut = new RequestMapper($this->mapper);
    }

    public function testGet()
    {
        $this->mapper->expects($this->once())
            ->method('map')
            ->with($this->isInstanceOf(GetRequest::class), null)
            ->willReturn(new \stdClass());

        $this->assertInstanceOf(\stdClass::class, $this->sut->get($this->requestQuery));
    }

    public function testCreate()
    {
        $this->mapper->expects($this->once())
            ->method('map')
            ->with($this->isInstanceOf(PostRequest::class), null)
            ->willReturn(new \stdClass());

        $this->assertInstanceOf(\stdClass::class, $this->sut->create($this->storeRequestQuery));
    }

    public function testUpdate()
    {
        $this->mapper->expects($this->once())
            ->method('map')
            ->with($this->isInstanceOf(PutRequest::class), null)
            ->willReturn(new \stdClass());

        $this->assertInstanceOf(\stdClass::class, $this->sut->update($this->storeRequestQuery));
    }

    public function testPatch()
    {
        $this->mapper->expects($this->once())
            ->method('map')
            ->with($this->isInstanceOf(PatchRequest::class), null)
            ->willReturn(new \stdClass());

        $this->assertInstanceOf(\stdClass::class, $this->sut->patch($this->storeRequestQuery));
    }

    public function testDelete()
    {
        $this->mapper->expects($this->once())
            ->method('map')
            ->with($this->isInstanceOf(DeleteRequest::class), null)
            ->willReturn(new \stdClass());

        $this->assertInstanceOf(\stdClass::class, $this->sut->delete($this->requestQuery));
    }

    public function testThrowsCorrectException()
    {
        $this->expectException(ClientException::class);

        $clientException = new ClientException(
            "Test exception",
            $this->createMock(RequestInterface::class)
        );
        $this->mapper->expects($this->once())
            ->method('map')
            ->with($this->isInstanceOf(GetRequest::class), null)
            ->willThrowException($clientException);

        $this->sut->get($this->requestQuery);
    }

    public function testThrowsCorrectNotFoundException()
    {
        $this->expectException(NotFoundHttpException::class);

        $clientException = new ClientException(
            "Test exception",
            $this->createMock(RequestInterface::class),
            new Response(403)
        );

        $this->mapper->expects($this->once())
            ->method('map')
            ->with($this->isInstanceOf(GetRequest::class), null)
            ->willThrowException($clientException);

        $this->sut->get($this->requestQuery);
    }
}
