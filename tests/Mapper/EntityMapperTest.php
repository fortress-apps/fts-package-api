<?php

namespace Fortress\Api\Tests\Mapper;

use Fortress\Api\Http\HttpClientInterface;
use Fortress\Api\Hydrator\HydratorInterface;
use Fortress\Api\Mapper\EntityMapper;
use Fortress\Api\Request\RequestInterface;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class EntityMapperTest extends TestCase
{
    public function testEntityMapper()
    {
        $client = $this->createMock(HttpClientInterface::class);
        $request = $this->createMock(RequestInterface::class);
        $hydrator = $this->createMock(HydratorInterface::class);
        $response = $this->createMock(Response::class);
        $entity = new \stdClass();

        $sut = new EntityMapper($client);

        $client->expects($this->once())
            ->method('makeRequest')
            ->with($request)
            ->willReturn($response);

        $response->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(200);

        $response->expects($this->once())
            ->method('getBody')
            ->willReturn(json_encode([]));

        $hydrator->expects($this->once())
            ->method('hydrate')
            ->with([])
            ->willReturn($entity);

        $this->assertEquals($entity, $sut->map($request, $hydrator));
    }

    public function testMapNoHydrator()
    {
        $client = $this->createMock(HttpClientInterface::class);
        $request = $this->createMock(RequestInterface::class);
        $response = $this->createMock(Response::class);

        $sut = new EntityMapper($client);

        $client->expects($this->once())
            ->method('makeRequest')
            ->with($request)
            ->willReturn($response);

        $response->expects($this->never())
            ->method('getStatusCode');

        $response->expects($this->never())
            ->method('getBody');

        $this->assertEquals($response, $sut->map($request));
    }
}
