<?php

namespace Fortress\Api\Tests\Http;

use Fortress\Api\Http\HttpJsonClient as JsonClient;
use Fortress\Api\Request\RequestInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class HttpJsonClientTest extends TestCase
{
    public function testMakeRequest()
    {
        $client = $this->createMock(ClientInterface::class);
        $request = $this->createMock(RequestInterface::class);

        $request->expects($this->once())
            ->method('getHeaders')
            ->willReturn(['X-Test' => true]);

        $request->expects($this->once())
            ->method('getData')
            ->willReturn(null);

        $request->expects($this->once())
            ->method('getUri')
            ->willReturn('/test');

        $request->expects($this->once())
            ->method('getMethod')
            ->willReturn('POST');

        $client->expects($this->once())
            ->method('request')
            ->with('POST', '/test', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'X-Test' => true
                ],
            ])
            ->willReturn(new Response);


        $sut = new JsonClient($client);
        $actual = $sut->makeRequest($request);

        $this->assertInstanceOf(ResponseInterface::class, $actual);
    }

    public function testMakeRequestWithData()
    {
        $client = $this->createMock(ClientInterface::class);
        $request = $this->createMock(RequestInterface::class);

        $request->expects($this->once())
            ->method('getHeaders')
            ->willReturn(['X-Test' => true]);

        $request->expects($this->exactly(2))
            ->method('getData')
            ->willReturn(['data' => 'data-test']);

        $request->expects($this->once())
            ->method('getUri')
            ->willReturn('/test');

        $request->expects($this->once())
            ->method('getMethod')
            ->willReturn('POST');

        $client->expects($this->once())
            ->method('request')
            ->with('POST', '/test', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'X-Test' => true
                ],
                'json' => [
                    'data' => 'data-test'
                ]
            ])
            ->willReturn(new Response);


        $sut = new JsonClient($client);
        $actual = $sut->makeRequest($request);

        $this->assertInstanceOf(ResponseInterface::class, $actual);
    }
}
